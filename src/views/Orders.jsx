import OrdersCoffeeButton from "../components/Orders/OrdersCoffeeButton"
import withAuth from "../hoc/withAuth"
import OrdersForm from "../components/Orders/OrdersForm"
import { useState } from "react"
import { useUser } from "../context/UserContext"
import { orderAdd } from "../api/order"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"

const COFFEES = [
  {
    id: 1,
    name: "Americano",
    image: "img/americano.png",
  },
  {
    id: 2,
    name: "Cappuccino",
    image: "img/cappuccino.png",
  },
  {
    id: 3,
    name: "Latte",
    image: "img/latte.png",
  },
  {
    id: 4,
    name: "Espresso",
    image: "img/espresso.png",
  },
]

const Orders = () => {
  const [coffee, setCoffee] = useState(null)
  const { user, setUser } = useUser()

  const handleCoffeeClicked = (coffeeId) => {
    setCoffee(COFFEES.find((coffee) => coffee.id === coffeeId))
  }

  const handleOrderCliked = async (notes) => {
    if (!coffee) {
      // check if we have a coffee
      alert("please select coffee first")
      return
    }

    // combine coffee with notes
    const order = (coffee.name + " " + notes).trim()

    // send http-request
    const [error, updatedUser] = await orderAdd(user, order)
    if (error !== null) {
      // bad
      return
    }

    // keep UI state and server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser)
    // update context-state
    setUser(updatedUser)

    console.log("Error", error)
    console.log("updatedUser", updatedUser)
  }

  const availableCoffees = COFFEES.map((coffee) => {
    return (
      <OrdersCoffeeButton
        key={coffee.id}
        coffee={coffee}
        onSelect={handleCoffeeClicked}
      />
    )
  })

  return (
    <>
      <h1>Orders</h1>
      <section id="coffee-options">{availableCoffees}</section>
      <section id="order-notes">
        <OrdersForm onOrder={handleOrderCliked} />
      </section>
      <h4>Summary:</h4>
      {coffee && <p>Selected coffee: {coffee.name}</p>}
    </>
  )
}
export default withAuth(Orders)

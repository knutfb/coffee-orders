import { Link } from "react-router-dom"
import { storageDelete, storageSave } from "../../utils/storage"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { orderClearHistory } from "../../api/order"

const ProfileActions = () => {
  const { user, setUser } = useUser()

  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      storageDelete(STORAGE_KEY_USER)
      setUser(null)
    }
  }

  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis cannot be undone")) {
      return
    }
    const [clearError] = await orderClearHistory(user.id)

    if (clearError !== null) {
      return
    }
    const updatedUser = {
      ...user,
      orders: [],
    }
    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }

  return (
    <ul>
      <li>
        <Link to="/orders">Orders</Link>
      </li>
      <li>
        <button onClick={handleClearHistoryClick}>Clear hisotry</button>
      </li>
      <li>
        <button onClick={handleLogoutClick}>Log out</button>
      </li>
    </ul>
  )
}
export default ProfileActions
